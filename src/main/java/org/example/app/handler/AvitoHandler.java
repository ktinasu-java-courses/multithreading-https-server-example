package org.example.app.handler;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.dto.*;
import org.example.app.exception.NotFoundItemException;
import org.example.app.manager.AvitoManager;
import org.example.framework.exception.MethodNotAllowedException;
import org.example.framework.handler.Handler;
import org.example.framework.http.Request;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class AvitoHandler {
    private final Gson gson;
    private final AvitoManager manager;

    public void getAll(final Request request, final OutputStream responseStream) throws IOException {
        final List<AvitoGetAllRS> responseDTO = manager.getAll();
        final byte[] responseBody = gson.toJson(responseDTO).getBytes(StandardCharsets.UTF_8);
        writeResponse(responseStream, responseBody);
    }

    public void getById(final Request request, final OutputStream responseStream) throws IOException {
        final String requestBody = new String(request.getBody(), StandardCharsets.UTF_8);
        final AvitoGetByIdRQ requestDTO = gson.fromJson(requestBody, AvitoGetByIdRQ.class);
        try {
            final AvitoGetByIdRS responseDTO = manager.getById(requestDTO);
            final byte[] responseBody = gson.toJson(responseDTO).getBytes(StandardCharsets.UTF_8);
            writeResponse(responseStream, responseBody);
        } catch (NotFoundItemException e) {
            Handler.methodNotAllowedHandler(request, responseStream);
        }
    }

    public void create(final Request request, final OutputStream responseStream) throws IOException {
        final String requestBody = new String(request.getBody(), StandardCharsets.UTF_8);
        final AvitoCreateRQ requestDTO = gson.fromJson(requestBody, AvitoCreateRQ.class);

        try {
            final AvitoCreateRS responseDTO = manager.create(requestDTO);
            final byte[] responseBody = gson.toJson(responseDTO).getBytes(StandardCharsets.UTF_8);
            writeResponse(responseStream, responseBody);
        } catch (MethodNotAllowedException e) {
            Handler.methodNotAllowedHandler(request, responseStream);
        }
    }

    public void update(final Request request, final OutputStream responseStream) throws IOException {
        final String requestBody = new String(request.getBody(), StandardCharsets.UTF_8);
        final AvitoUpdateRQ requestDTO = gson.fromJson(requestBody, AvitoUpdateRQ.class);
        try {
            final AvitoUpdateRS responseDTO = manager.update(requestDTO);
            final byte[] responseBody = gson.toJson(responseDTO).getBytes(StandardCharsets.UTF_8);
            writeResponse(responseStream, responseBody);
        } catch (NotFoundItemException | MethodNotAllowedException e) {
            Handler.methodNotAllowedHandler(request, responseStream);
        }
    }

    public void deleteById(final Request request, final OutputStream responseStream) throws IOException {
        final String requestBody = new String(request.getBody(), StandardCharsets.UTF_8);
        final AvitoDeleteByIdRQ requestDTO = gson.fromJson(requestBody, AvitoDeleteByIdRQ.class);
        try {
            final AvitoDeleteByIdRS responseDTO = manager.deleteById(requestDTO);
            final byte[] responseBody = gson.toJson(responseDTO).getBytes(StandardCharsets.UTF_8);
            writeResponse(responseStream, responseBody);
        } catch (NotFoundItemException | MethodNotAllowedException e) {
            Handler.methodNotAllowedHandler(request, responseStream);
        }
    }

    private void writeResponse(OutputStream responseStream, byte[] body) throws IOException {
        responseStream.write((
                "HTTP/1.1 200 Ok\r\n" +
                        "Content-Length: " + body.length + "\r\n" +
                        "Connection: close\r\n" +
                        "Content-Type: application/json\r\n" +
                        "\r\n"
        ).getBytes(StandardCharsets.UTF_8));
        responseStream.write(body);
    }
}
