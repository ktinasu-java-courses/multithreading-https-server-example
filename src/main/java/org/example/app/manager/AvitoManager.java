package org.example.app.manager;

import org.example.app.domain.Apartment;
import org.example.app.dto.*;
import org.example.app.exception.NotFoundItemException;
import org.example.framework.auth.SecurityContext;
import org.example.framework.auth.principal.LoginPrincipal;
import org.example.framework.exception.MethodNotAllowedException;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


public class AvitoManager {
    private final List<Apartment> items = new ArrayList<>(100);

    public synchronized List<AvitoGetAllRS> getAll() {
        return items.stream()
                .map(o -> new AvitoGetAllRS(
                        o.getId(),
                        o.getOwner(),
                        o.getRoomsNumber(),
                        o.isStudio(),
                        o.isFreeLayout(),
                        o.getPrice(),
                        o.getArea(),
                        o.isHasBalcony(),
                        o.isHasLoggia(),
                        o.getFloorNumber(),
                        o.getFloorNumberInHouse(),
                        o.getAddress(),
                        o.getDescription()
                ))
                .collect(Collectors.toList());
    }

    public synchronized AvitoGetByIdRS getById(final AvitoGetByIdRQ requestDTO) {
        return items.stream()
                .filter(o -> o.getId().equals(requestDTO.getId()))
                .findAny()
                .map(o -> new AvitoGetByIdRS(o.getId(),
                        o.getOwner(),
                        o.getRoomsNumber(),
                        o.isStudio(),
                        o.isFreeLayout(),
                        o.getPrice(),
                        o.getArea(),
                        o.isHasBalcony(),
                        o.isHasLoggia(),
                        o.getFloorNumber(),
                        o.getFloorNumberInHouse(),
                        o.getAddress(),
                        o.getDescription()))
                .orElseThrow(() -> new NotFoundItemException("item not found"));
    }

    public AvitoCreateRS create(final AvitoCreateRQ requestDTO) {
        final Principal principal = SecurityContext.getPrincipal();

        if (!(principal instanceof LoginPrincipal)) {
            throw new MethodNotAllowedException("not a login principal");
        }

        final Apartment item = new Apartment(
                UUID.randomUUID().toString(),
                principal.getName(),
                requestDTO.getRoomsNumber(),
                requestDTO.isStudio(),
                requestDTO.isFreeLayout(),
                requestDTO.getPrice(),
                requestDTO.getArea(),
                requestDTO.isHasBalcony(),
                requestDTO.isHasLoggia(),
                requestDTO.getFloorNumber(),
                requestDTO.getFloorNumberInHouse(),
                requestDTO.getAddress(),
                requestDTO.getDescription()
        );

        synchronized (this) {
            items.add(item);

            return new AvitoCreateRS(
                    item.getId(),
                    item.getRoomsNumber(),
                    item.isStudio(),
                    item.isFreeLayout(),
                    item.getPrice(),
                    item.getArea(),
                    item.isHasBalcony(),
                    item.isHasLoggia(),
                    item.getFloorNumber(),
                    item.getFloorNumberInHouse(),
                    item.getAddress(),
                    item.getDescription()
            );
        }
    }

    public AvitoUpdateRS update(final AvitoUpdateRQ requestDTO) {
        final Principal principal = SecurityContext.getPrincipal();
        final Optional<Apartment> item;
        synchronized (this) {
            item = items.stream()
                    .filter(o -> o.getId().equals(requestDTO.getId()))
                    .findAny();
        }
        item.orElseThrow(() -> new NotFoundItemException("item not found"));
        userValidityCheck(principal, item.get());

        synchronized (this) {
            final int idx = getIdxById(requestDTO.getId());
            if (idx == -1) {
                throw new NotFoundItemException("item not found");
            }
            items.set(idx, new Apartment(requestDTO.getId(),
                    principal.getName(),
                    requestDTO.getRoomsNumber(),
                    requestDTO.isStudio(),
                    requestDTO.isFreeLayout(),
                    requestDTO.getPrice(),
                    requestDTO.getArea(),
                    requestDTO.isHasBalcony(),
                    requestDTO.isHasLoggia(),
                    requestDTO.getFloorNumber(),
                    requestDTO.getFloorNumberInHouse(),
                    requestDTO.getAddress(),
                    requestDTO.getDescription()));

            return new AvitoUpdateRS(
                    requestDTO.getId(),
                    requestDTO.getRoomsNumber(),
                    requestDTO.isStudio(),
                    requestDTO.isFreeLayout(),
                    requestDTO.getPrice(),
                    requestDTO.getArea(),
                    requestDTO.isHasBalcony(),
                    requestDTO.isHasLoggia(),
                    requestDTO.getFloorNumber(),
                    requestDTO.getFloorNumberInHouse(),
                    requestDTO.getAddress(),
                    requestDTO.getDescription()
            );
        }
    }

    public AvitoDeleteByIdRS deleteById(final AvitoDeleteByIdRQ requestDTO) {
        final Optional<Apartment> item;
        synchronized (this) {
            item = items.stream()
                    .filter(o -> o.getId().equals(requestDTO.getId()))
                    .findAny();
        }
        item.orElseThrow(() -> new NotFoundItemException("item not found"));
        final Apartment apartment = item.get();
        userValidityCheck(SecurityContext.getPrincipal(), apartment);

        synchronized (this) {
            int idx = getIdxById(requestDTO.getId());
            if (idx == -1) {
                throw new NotFoundItemException("item not found");
            }
            items.remove(idx);
            return new AvitoDeleteByIdRS(
                    apartment.getId(),
                    apartment.getRoomsNumber(),
                    apartment.isStudio(),
                    apartment.isFreeLayout(),
                    apartment.getPrice(),
                    apartment.getArea(),
                    apartment.isHasBalcony(),
                    apartment.isHasLoggia(),
                    apartment.getFloorNumber(),
                    apartment.getFloorNumberInHouse(),
                    apartment.getAddress(),
                    apartment.getDescription()
            );
        }
    }

    private void userValidityCheck(Principal principal, Apartment item) {
        if (!(principal instanceof LoginPrincipal)) {
            throw new MethodNotAllowedException("not a login principal");
        }
        if (!principal.getName().equals(item.getOwner())) {
            throw new MethodNotAllowedException("not an owner");
        }
    }

    private synchronized int getIdxById(String id) {
        int idx = -1;
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getId().equals(id)) {
                idx = i;
            }
        }
        return idx;
    }
}
