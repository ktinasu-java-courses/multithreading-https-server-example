package org.example.app.manager;

import lombok.extern.slf4j.Slf4j;
import org.example.app.dto.UserRegisterRQ;
import org.example.app.dto.UserRegisterRS;
import org.example.framework.auth.Authentication;
import org.example.framework.auth.Authenticator;
import org.example.framework.auth.LoginPasswordAuthentication;
import org.example.framework.exception.BadRegisterDataException;
import org.example.framework.exception.LoginAlreadyRegisteredException;
import org.example.framework.exception.UnsupportAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class UserManager implements Authenticator {
    private final Map<String, String> users = new HashMap<>();
    private final PasswordEncoder encoder;

    private List<String> listLogins;
    private List<String> listPasswords;

    public UserManager(final PasswordEncoder encoder) {
        this.encoder = encoder;
        readFiles();
    }

    public UserRegisterRS create(final UserRegisterRQ requestDTO) throws NoSuchFileException {
        final String login = requestDTO.getLogin().trim().toLowerCase();
        final String password = requestDTO.getPassword().trim().toLowerCase();

        try {
            final boolean matchLogin = listLogins.stream().anyMatch(o -> o.equals(login));
            if (matchLogin) {
                throw new BadRegisterDataException("Bad login");
            }
            final boolean matchPassword = listPasswords.stream().anyMatch(o -> o.equals(password));
            if (matchPassword) {
                throw new BadRegisterDataException("Password is in 10 000 leaked passwords");
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            throw new NoSuchFileException("Can't find file");
        }

        final String encodedPassword = encoder.encode(requestDTO.getPassword());

        synchronized (this) {
            if (users.containsKey(login)) {
                log.error("registration with same login twice: {}", login);
                throw new LoginAlreadyRegisteredException();
            }
            users.put(login, encodedPassword);
        }
        return new UserRegisterRS(login);
    }

    @Override
    public boolean authenticate(final Authentication request) {
        if (!(request instanceof LoginPasswordAuthentication)) {
            throw new UnsupportAuthenticationToken();
        }

        final LoginPasswordAuthentication converted = (LoginPasswordAuthentication) request;
        final String login = converted.getLogin();
        final String password = (String) converted.getCredentials();

        final String encodedPassword;
        synchronized (this) {
            if (!users.containsKey(login)) {
                return false;
            }

            encodedPassword = users.get(login);
        }
        return encoder.matches(password, encodedPassword);
    }

    private void readFiles() {
        try {
            listLogins = Files.newBufferedReader(Paths.get("bad-logins.txt"))
                    .lines()
                    .collect(Collectors.toList());
            listPasswords = Files.newBufferedReader(Paths.get("leaked-passwords.txt"))
                    .lines()
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
