package org.example.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AvitoGetByIdRS {
    private String id;
    private String owner;
    private int roomsNumber;
    private boolean studio;
    private boolean freeLayout;
    private int price;
    private double area;
    private boolean hasBalcony;
    private boolean hasLoggia;
    private int floorNumber;
    private int floorNumberInHouse;
    private String address;
    private String description;
}
