package org.example.app;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.example.app.handler.AvitoHandler;
import org.example.app.handler.UserHandler;
import org.example.app.manager.AvitoManager;
import org.example.app.manager.UserManager;
import org.example.framework.http.HttpMethods;
import org.example.framework.http.Server;
import org.example.framework.middleware.AnonAuthMiddleware;
import org.example.framework.middleware.bodyauth.JSONBodyAuthNMiddleware;
import org.example.framework.util.Maps;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;

import java.util.regex.Pattern;

@Slf4j
public class Main {
    public static void main(String[] args) {
        System.setProperty("javax.net.ssl.keyStore", "web-certs/server.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", "passphrase");


        final Argon2PasswordEncoder passwordEncoder = new Argon2PasswordEncoder();
        final UserManager userManager = new UserManager(passwordEncoder);
        final AvitoManager manager = new AvitoManager();

        final Gson gson = new Gson();
        final AvitoHandler avitoHandler = new AvitoHandler(gson, manager);
        final UserHandler userHandler = new UserHandler(gson, userManager);

        final String regex = "(?<apartmentId>[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})$";
        final Server server = Server.builder()
                .middleware(new JSONBodyAuthNMiddleware(gson, userManager))
                .middleware(new AnonAuthMiddleware())
                .routes(
                        Maps.of(
                                Pattern.compile("^/avito$"), Maps.of(
                                        HttpMethods.GET, avitoHandler::getAll,
                                        HttpMethods.POST, avitoHandler::create
                                ),
                                Pattern.compile("^/avito/" + regex), Maps.of(
                                        HttpMethods.GET, avitoHandler::getById,
                                        HttpMethods.DELETE, avitoHandler::deleteById,
                                        HttpMethods.PUT, avitoHandler::update
                                ),
                                Pattern.compile("^/register$"), Maps.of(
                                        HttpMethods.POST, userHandler::register
                                ),
                                Pattern.compile("^/auth$"), Maps.of(
                                        HttpMethods.POST, userHandler::authenticate
                                )
                        )
                )
                .build();

        final int port = 8443;
        try {
            server.start(port);
        } catch (Exception e) {
            log.error("can't serve", e);
        }
    }
}
